import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import ManageProfile from '../screens/Settings/ManageProfile';
import SettingScreen from '../screens/Settings/SettingsScreen';
import ChangePassword from '../screens/Settings/ChangePassword';
import test from '../screens/Settings/Testing/index'
import HealthDeclare from '../screens/Settings/HealthDeclare';
import MovingHistory from '../screens/Settings/MovingHistory/index'

export default function (props){
    const Stack = createStackNavigator();
    const {navigation} = props;
    console.log("HIH",navigation);
    return (
        <Stack.Navigator >
            <Stack.Screen
                name = {"root"}
                component = {SettingScreen}
                options = {{title : "Cá nhân"}}
                
            />

            <Stack.Screen
                name = "ManageProfile"
                component = {ManageProfile}
                options = {{title : "Quản lý thông tin"}}

            />

            <Stack.Screen
                name = "ChangePassword"
                component = {ChangePassword}
                options = {{title : "Thay đổi mật khẩu"}}

            />

            <Stack.Screen
                name = "MovingHistory"
                component = {MovingHistory}
                options = {{title : "Lịch sử di chuyển"}}

            />

            <Stack.Screen
                name = "TestingManagement"
                // component = {TestingManagement}
                component = {test}
                options = {{title : "Quản lý xét nghiệm"}}
            />

            <Stack.Screen
                name = "HealthDeclare"
                // component = {HealthDeclare}
                options = {{title : "Khai báo y tế"}}

            >
                {navigation=><HealthDeclare tabBarNavigation = {navigation}></HealthDeclare>}
            </Stack.Screen>


        </Stack.Navigator>
    )
}