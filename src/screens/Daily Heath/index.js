import React from 'react';
import {View,Text,StyleSheet} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import info from './info';
import register from './register';

const Tab = createMaterialTopTabNavigator();

export default function (props) {
    return (
        <Tab.Navigator>
            <Tab.Screen name="info" component={info} 
                options = {{tabBarLabel : "Lịch sử đăng ký"}}
            />
            <Tab.Screen name="register" component={register} 
                options = {{tabBarLabel : "Đăng ký xét nghiệm"}}

            />
        </Tab.Navigator>

    )
}