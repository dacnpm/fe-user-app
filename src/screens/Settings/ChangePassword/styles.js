import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'

const styles = StyleSheet.create({
    container : {
        ..._styles.container,
    },
    
    header: {
        ..._styles.header
    },

    row : {
        flexDirection : 'row',
        alignItems : 'center',
        paddingVertical : 5,
        paddingHorizontal : 15,
    },

    groupButtons : {
        marginTop   : 20,
        marginBottom : 50,
        justifyContent : 'space-around',

    },

    submitButton : {
        ..._styles.colorButton,
        paddingHorizontal : 20,
        paddingVertical : 5,

    },

    backButton : {
        ..._styles.cancelButton,
        paddingHorizontal : 20,
        paddingVertical : 5,
    }
    
   
})

export default styles;