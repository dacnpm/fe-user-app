import React, {useState,useEffect,useReducer} from 'react';
import {View,Text,StyleSheet, FlatList} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import { Card, ListItem,Image } from 'react-native-elements'


import testingApi from '../../../api/testingApi';
import AsyncStorage from '../../../storage';
import userApi from '../../../api/userApi'
import TestingResult from '../../../components/TestingResult';
import { ScrollView } from 'react-native-gesture-handler';
import { Alert } from 'react-native';
export default function (props) {
    const {navigation} = props; 
    const [data,setData] = useState([]);
    const [update,setUpdate] = useState(0);

    const [isLoading,setIsLoading] = useState(true);
    useEffect(()=>{
        console.log("RENDER LAI NE");
        AsyncStorage.read()
            .then(token=>{
                return userApi.testingResult(token)
            .then(res=>{
                const {data} = res
                setData(data);
            })
                
            })
            .catch(err=>{
                console.log(err);
            })
            .finally(
                ()=>{
                    setIsLoading(false)
                }
            )
    },[])

    useEffect(() => {
        const unsubscribe = navigation.addListener('tabPress', e => {
          // Prevent default behavior
          setUpdate(update+1);
          AsyncStorage.read()
            .then(token=>{
                return userApi.testingResult(token)
            .then(res=>{
                const {data} = res
                setData(data);
                console.log("DATA",data);
            })
                
            })
            .catch(err=>{
                console.log(err);
            })
            .finally(
                ()=>{
                    setIsLoading(false)
                }
            )
          
          // ...
        });
      
        return unsubscribe;
      }, []);
    return (
        <ScrollView>
        <Spinner
          visible={isLoading}
        />
        <View style ={styles.container}>
            <Text style = {styles.header}> Thông tin xét nghiệm</Text>

            <View style={styles.infoContainer}>
                <FlatList
                    data = {data}
                    renderItem = {({item})=>(
                        <TestingResult
                            item = {item}
                        />
                    )}
                    keyExtractor ={(item,index)=> index+""}
                />
            </View>

        </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor : 'white',
        paddingHorizontal : 10,
    },

    header : {
        fontWeight : 'bold',
        fontSize : 30,
        textAlign : 'center',
        paddingTop : 30,
        // backgroundColor : 'pink'
    },

    infoContainer : {
        paddingTop : 30,

    },

    dataContainer : {
        display : 'flex',
        flexDirection : 'row',
    },

    title : {
        fontSize : 25,  
        fontWeight : 'bold',
    },

    result :{
    }
})