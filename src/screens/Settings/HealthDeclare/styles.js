import {StyleSheet} from 'react-native'
import _styles from '../../../styles/style'

const styles = StyleSheet.create({
    container : {
        flex : 1,   
        paddingHorizontal : 10,
        backgroundColor : 'white',
        paddingBottom : 30,
    },
    
    header : {
        fontSize : 30,
        fontWeight : 'bold',
        textAlign : 'center',
        paddingVertical : 20,
    },

    row : {
        flexDirection : 'row',
        alignItems  : 'center',
    },

    title : {
        fontWeight : 'bold',
        fontSize : 18,
    },

    inputContainer :{
        // borderWidth : 1,
        // borderRadius  : 20,
        // marginHorizontal : 10, 
        // marginVertical : 20,
        // paddingHorizontal : 20,
        paddingVertical : 10,
        paddingHorizontal : 15,
    },

    label : {
        fontSize : 20,
        fontWeight : 'bold',
    },

    titleStyle : {
        ..._styles.colorButton,
        paddingHorizontal : 30,
        marginVertical : 20,
    },

    borderPicker: {
        ..._styles.borderPicker,
        marginHorizontal : 10,
    },

    datetimeContainer : {
        ..._styles.borderPicker,
        marginHorizontal : 10,
        flexDirection : 'row'
        ,paddingVertical : 10,
        alignItems : 'center',
        justifyContent : 'space-around'
    },
    
    textInput: {
        paddingHorizontal : 15
    },

    disableDate : {
        backgroundColor : 'grey',
        color: 'black',
    }
})

export default styles;