import React, { useCallback, useEffect, useState } from 'react';
import { StyleSheet, Text, View,RefreshControl } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import userApi from '../../../api/userApi';
import ItineraryHistory from '../../../components/ItineraryHistory';
import _styles from '../../../styles/style'
import AsyncStorage from '../../../storage'
import { ScrollView } from 'react-native';


export default function (props){
    const [data,setData] = useState([]);
    const [mustUpdate,setMustUpdate] = useState(false);
    const [isLoading,setIsLoading] = useState (false);
    const [refreshing, setRefreshing] = useState(false);
    const [list1,setList1] = useState([])
    const [list2,setList2] = useState([])
    const {tabBarNavigation} = props;


    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const onRefresh = useCallback(() => {
        setRefreshing(true);
        wait(500).then(() => setRefreshing(false));

        setMustUpdate(!mustUpdate)
      }, [mustUpdate]);

    useEffect(()=>{
        AsyncStorage.read()
            .then(token=>{
                return userApi.GetItinerary(token);
            })
            .then(res=>{
                const {data} = res;
                console.log("data",data);
                
                setData(data);
                setList1(data.map(i=>new Date(i.departureTime).getTime()))
                setList2(data.map(i=>new Date(i.landingTime).getTime()))
                // console.log("Xxxx",list1);
            })
            .catch(err=>console.log(err));
    },[mustUpdate])

    

    return (
        <ScrollView style = {styles.container}
        refreshControl={
            <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
            />
        }>

            <Text style = {styles.header}>Lịch trình di chuyển</Text>

            {/* <FlatList
                
                data = {data}
                renderItem = {({item})=>(
                    <ItineraryHistory 
                         listTime = {[list1,list2]}
                         item={item}
                         data = {data}
                         setData = {setData}
                         mustUpdate = {mustUpdate}
                         setMustUpdate = {setMustUpdate}
                         tabBarNavigation = {tabBarNavigation}
                    />
                )}
                keyExtractor = {(item,index)=>index+""}

            /> */}
            {data.map(item=>(
                 <ItineraryHistory 
                 listTime = {[list1,list2]}
                 item={item}
                 data = {data}
                 setData = {setData}
                 mustUpdate = {mustUpdate}
                 setMustUpdate = {setMustUpdate}
                 tabBarNavigation = {tabBarNavigation}
            />
            ))}
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'white',
        paddingHorizontal : 10,
        paddingVertical : 30,
    },

    header : {
        ..._styles.header
    }
})