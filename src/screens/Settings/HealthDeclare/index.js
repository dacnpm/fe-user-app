import React from 'react';
import {View,Text,StyleSheet} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Screen1 from './screen1';
import Screen2 from './screen2';

const Tab = createMaterialTopTabNavigator();

export default function (props) {
    const {tabBarNavigation} = props;
    console.log("Xxx",tabBarNavigation);
    return (
        <Tab.Navigator>
            <Tab.Screen 
                name="Khaibao" 
                options = {{tabBarLabel : "Khai báo y tế"}}
            >
                {tabBarNavigation=>
                    <Screen1
                        tabBarNavigation = {tabBarNavigation}
                        >   
                    </Screen1>
                    }
            </Tab.Screen>
            <Tab.Screen 
                name="History123" 
                options = {{tabBarLabel : "Lịch trình di chuyển"}}
            >
                {tabBarNavigation=>
                 <Screen2 tabBarNavigation = {tabBarNavigation}
                 />
                }
            </Tab.Screen>
        </Tab.Navigator>

    )
}