import React, { useState,useEffect} from 'react';
import { Alert, FlatList, StyleSheet, Text, TouchableOpacity, View,SafeAreaView,Image } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';


import userApi from '../../api/userApi';
// import CategoryListItem from '../components/CategoryListItem';
import NewsItem from '../../components/NewsItem'
import AsyncStorage from '../../storage';
import styles from './styles'

export default function (props) {
  const {navigation} = props;
  // console.log(navigation); 
  const [isLoading,setIsLoading] = useState(true);
  const [data,setData] =useState( [

      ]);
  
  useEffect(()=>{
    userApi.getNews()
        .then(res=>{
            console.log(res);
            setData(res.data)
          })
          .catch(err=>{
            console.error(err);
          })
          .finally(()=>{
            setIsLoading(false);
          })
        .catch(err=>{
          console.log(err);
        })
  },[])

  function onItemClick(item){
    console.log(item.content.indexOf("\n"));
    navigation.navigate('Details',item);
  }
      


    return (

        <SafeAreaView style={styles.container}>
        <Spinner
          visible={isLoading}
          textContent={'Loading...'}
        />
          <FlatList
            data={data}
            renderItem={({item}) => (
              <View
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  margin: 1
                }}>
                {/* <Image
                  style={styles.imageThumbnail}
                  source={{uri: item.src}}
                /> */}
                <NewsItem
                    onClick = {()=>{
                      onItemClick(item);
                    }}
                    item = {item}
                />
              </View>
            )}
            //Setting the number of column
            numColumns={1}
            keyExtractor={(item, index) => index}
          />
        </SafeAreaView>

      // </View>
    )
}
