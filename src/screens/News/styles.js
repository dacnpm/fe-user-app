import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      backgroundColor: 'white',
      borderTopWidth : 2,
      paddingHorizontal : 10,
    },
    imageThumbnail: {
      justifyContent: 'center',
      alignItems: 'center',
      height: 150,
    },
  });
  