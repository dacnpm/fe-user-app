import React, { useContext, useState,useRef, useEffect } from 'react';
import { Alert, StyleSheet, Text, View } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';


import Button from '../../components/Button';
import Input from '../../components/Input';
import Logo from '../../components/Logo';
import AuthContext from '../../context/AuthContext';
import _styles from '../../styles/style'
import loginApi from '../../api/loginApi'
import ComfirmCodeModal from '../../components/Modal/ConfirmPass'
import ChangePassModal from '../../components/Modal/ChangePass'
import * as constants from '../../constants'
import validation from '../../utils/validation';
import validation1 from '../../utils/validation1';


export default function NewFormLogin(props) {
    const [loading,setLoading] = useState(false);
    const [email,setEmail] = useState("");
    const {signIn } = useContext(AuthContext);
    const refModal = useRef(null);
    const {navigation} = props;
    const [isEmailErr,setIsEmailErr] = useState(false);

    useEffect(()=>{
      setIsEmailErr(!validation1.isEmail(email));
    })

    const onPress = () =>{
      if (isEmailErr) {
        Alert.alert("Email không hợp lệ");
        return;
      }
      setLoading(true);
      loginApi.forgotPasswordCode({email})
      .then(res=>{
        console.log(res);
        if (res.isSuccess){
          navigation.navigate("ChangePassword",{email})
        }
      })
      .catch(err=>{
        console.log(err);
      })
      .finally(()=>{
        setLoading(false);
      })
      
    }

    return (
      <View style = {{..._styles.container,...styles.container}}>
          <Spinner
            visible={loading}
          />
          <Logo></Logo>
          <Input 
            label ={"Email"} 
            iconName = {"mail-outline"}  
            onChangeText = {setEmail} 
            keyboardType= {"email-address"}
            isErr = {isEmailErr}
            errMsg={constants.EmailErrMsg} 

          />
          <ChangePassModal
             ref={refModal} email = {email} 
          ></ChangePassModal>
          <Button  
            title = {"Lấy mã xác nhận"} 
            titleStyle={styles.title} 
            onPress = {onPress}
          ></Button>


        </View>
  )
  }


  const styles = StyleSheet.create({
    container : {
      flex : 1,
      backgroundColor : 'white',
    },

    title : {
        marginTop : 20,
      ..._styles.colorButton,
      width : 300,
    }
   

})

