import React, { useEffect, useState } from 'react';
import { Alert, KeyboardAvoidingView, ScrollView, StyleSheet, Text, View } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import loginApi from '../../api/loginApi';
import Button from '../../components/Button/index';
import Input from '../../components/Input';
import _styles from '../../styles/style';
import * as constants from '../../constants'
import validation from '../../utils/validation';
import validation1 from '../../utils/validation1';

export default function (props) {
    const {email} = props.route.params;
    const [password,setPassword] = useState("");
    const [conFirmPass,setConFirmPass] = useState("");
    const [code,setCode] = useState("");
    const [loading,setLoading] = useState(false);
    const [isPasswordErr,setIsPasswordErr] = useState(false);
    const [isConfirmPasswordErr,setIsConfirmPasswordErr] = useState(false);
    const {navigation} = props;
    useEffect(()=>{
      setIsPasswordErr(!validation1.isPassword(password));
      setIsConfirmPasswordErr(!validation1.isComfirmPassword(password,conFirmPass))
    })

    const onPress = () =>{
        if (isPasswordErr ||isConfirmPasswordErr ){
          Alert.alert("Mật khẩu không hợp lệ");
          return;
        }
        setLoading(true);
        loginApi.forgotPassword({email,password,code})
          .then(res=>{
            console.log(res);
            Alert.alert("Đổi mật khẩu thành công"," ",
                [
                  {
                  text :"OK",
                  onPress : ()=>{
                      navigation.popToTop();
                    }
                  }
                ]
            );

          })
          .catch(err=>{
            console.log(err);
          })
          .finally(()=>{
            setLoading(false);
          })
    }

    return (
      <ScrollView>
      <View style = {{..._styles.container,...styles.container}}>
            <Spinner
              visible={loading}
            />
          {/* <Logo></Logo> */}
            <Text style = {styles.title}>Thay đổi mật khấu</Text>
            <KeyboardAvoidingView
              behavior = 'position'
              keyboardVerticalOffset = {50}
            >
            <Input  editable={false} label ={"Email"} iconName = {"mail-outline"} value ={email}  onChangeText = {()=>{}} keyboardType= {"email-address"}  ></Input>
            <Input 
              label ={"Mật khẩu"} 
              iconName = {"key-outline"} 
              secureTextEntry= {true}  
              onChangeText = {setPassword} 
              isErr = {isPasswordErr}
              errMsg={constants.PasswordErrMsg}
            />
            <Input 
              label ={"Xác nhận mật khẩu"} 
              placeholder= {"Nhập lại Mật khẩu"} 
              iconName = {"key-outline"} secureTextEntry= {true}  
              onChangeText = {setConFirmPass} 
              isErr = {isConfirmPasswordErr}
              errMsg={constants.ConfirmPasswordErrMsg}
            />

            <Input 
              label ={"Email code"}  
              iconName = {"keypad-outline"} 
              onChangeText = {setCode}
            />
            </KeyboardAvoidingView>
            <Button onPress={onPress} title = {"Đổi mật khẩu"} titleStyle = {styles.button}></Button>

        </View>
        </ScrollView>
  )
}


  const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'white',
        paddingTop : 50,
        paddingHorizontal : 10,
    },
    button : {
      backgroundColor : "red",
      width : 250,
      marginVertical : 20,
      ..._styles.colorButton,
    },
    title : {
      color : 'red',
      textAlign : 'center',
      fontSize : 30,
    }
   
})

