import {StyleSheet} from 'react-native'
import { withSafeAreaInsets } from 'react-native-safe-area-context';

const styles = StyleSheet.create({
    colorButton : {
        backgroundColor : "#12e335",
        paddingVertical : 5,
    },
    container : {
        backgroundColor : 'white',
        padding : 10,
        flex : 1,
    },

    backgroundBorder : {
        backgroundColor : 'rgba(3, 207, 252,0.1)',
    },

    header : {
        fontSize : 25,
        fontWeight : 'bold',
        textAlign : 'center',
        paddingVertical : 10,
    },
    cancelButton : {
        backgroundColor : 'rgba(128, 143, 145,0.3)'
    },

    borderPicker : {
        borderWidth : 1,
        borderRadius : 10,
    },

    box : {
        paddingVertical : 10,
    },
})

export default styles;