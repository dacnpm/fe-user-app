import { Header } from "react-native/Libraries/NewAppScreen";
import axiosClient from "./axiosClient"

const baseURL = "/User";
 const userApi = {
    getProfile :  (token)=>{
        const url = baseURL+"/profile";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        }
        return axiosClient.get(url,config);
    },
    getNews :  ()=>{
        const url = "/Homepage/allnews";
        return axiosClient.get(url);
    },

    registerTesting : (token) => {
        // console.log("12312312",params);
        console.log("99",token);
        const data = {
            testingLocationId: 9,
            registerDate: "2021-05-03T08:55:37.670Z",
            testingDate: "2021-05-03T08:55:37.670Z"
          }
        const url = "/User/tesing/register";
        console.log("url:",url);
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        }
        
        return axiosClient.post(url,data,config)
    },

    medicalInfo : (data,token) =>{
        const url = baseURL + "/testing/medicalinfo";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.post(url,data,config);
    },

    testingResult : (token) =>{
        const url = baseURL + "/testing"
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.get(url,config);

    },

    UpdateItinerary : (token,data) => {
        const url = baseURL + "/itinerary";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.post(url,data,config)
    },

    GetItinerary : (token) =>{
        const url = baseURL + "/itinerary";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.get(url,config)
    },

    CheckInLocation : (token,data ) =>{
        const url = baseURL + "/itinerary/location-checkin";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.post(url,data,config)
    },

    MovingHistory : (token) =>{
        const url = baseURL + "/itinerary/location-checkin";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.get(url,config)

    },

    getProfile : (token)=>{
        const url = baseURL + "/profile";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.get(url,config)
    },

    UpdateProfile : (data,token)=>{
        const url = baseURL + "/profile";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.put(url,data,config);
    },

    ChangePassword : (data,token) =>{
        const url = baseURL + "/profile/password";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.put(url,data,config)
    },

    DeleteCheckIn : (idLocation,token)=>{
        const url = baseURL + "/itinerary/location-checkin/"+ idLocation;
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.delete(url,config);
    },
    EditCheckIn : (idLocation,data,token)=>{
        const url = baseURL + "/itinerary/location-checkin/"+ idLocation;
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.put(url,data,config);
    },

    DeleteItinerary : (idLocation,token)=>{
        const url = baseURL + "/itinerary/"+ idLocation;
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.delete(url,config);
    },

    EditItinerary : (idLocation,data,token)=>{
        const url = baseURL + "/itinerary/"+ idLocation;
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.put(url,data,config);
    },

    UploadAvatar : (token,file) =>{
        const url = baseURL + "/profile/avatar";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        let formData = new FormData();
        formData.append("avatar",file)
        return axiosClient.post(url,formData,config)

    },

    GetAvatar : (token) =>{
        const url = baseURL + "/profile/avatar";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        };
        return axiosClient.get(url,config)

    }


}
export default userApi;