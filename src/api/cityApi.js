import { Header } from "react-native/Libraries/NewAppScreen";
import axiosClient from "./axiosClient"
import AsyncStorage from '../storage'
import { Alert } from "react-native";

const baseURL = "/city";
 const cityApi = {
    getCities :  (token)=>{
        const url = baseURL;
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        }
        return axiosClient.get(url,config);
    },



}
export default cityApi;