import { Header } from "react-native/Libraries/NewAppScreen";
import axiosClient from "./axiosClient"
const baseURL = "/Authorization";

const loginApi = {
    login : (params) =>{
        const url = baseURL+"/user/login";
        console.log("params:",params);
        return axiosClient.post(url,params);
    },

    register : (params) =>{
        const url = baseURL+"/register";
        return axiosClient.post(url,params);
    },
    test : ()=>{
        const url = "/User/profile";
        return axiosClient.get(url);
    },

    verifyEmail : (params) =>{
        const url = baseURL+"/email";
        console.log("From API:",params);
        return axiosClient.post(url,params);
    },

    forgotPassword : (params) =>{
        const url = baseURL+"/forgotpassword";
        return axiosClient.post(url,params);
    },

    forgotPasswordCode : (params)=>{
        const url = baseURL+"/forgotpassword/code";
        return axiosClient.post(url,params);
        
    }
}
export default loginApi;