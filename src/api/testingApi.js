import { Header } from "react-native/Libraries/NewAppScreen";
import axiosClient from "./axiosClient"
import AsyncStorage from '../storage'
import { Alert } from "react-native";

const baseURL = "/testing";
 const testingApi = {
    getLocations :  (token)=>{
        const url = baseURL+"/location";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        }
        return axiosClient.get(url,config);
    },
    register : (token,params)=>{
        const url = "/User/testing/register";
        const config = {
            headers: { Authorization: `Bearer ${token}` }
        }
        console.log("PARAMS: ",params);
        return axiosClient.post(url,params,config);
    }

}
export default testingApi;