import React from 'react';
import {
    Switch, Text,

    View
} from 'react-native';
import ToggleSwitch from 'toggle-switch-react-native'
import style from './style';


import styles from './style';



export default function(props){
    const {item} = props;
    const {status,setStatus} = item;
    // console.log(item.content);
 
    return (
        <View style={styles.container}>
  
            <Text style = {style.label}>{item.vie}</Text>
            <ToggleSwitch
                animationSpeed	= {150}
                isOn={status}
                onColor='#00b80f'	
                offColor='#ecf0f1'
                // label={item.vie}
                // labelStyle={styles.label}
                size={"medium"}
                onToggle={isOn =>  setStatus(!status)}
            
            />
        </View>
    )
}
