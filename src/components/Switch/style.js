import { StyleSheet } from "react-native";

export default styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        flexDirection : 'row',
        margin : 10,
      },

    title : {
        fontSize : 20,
        fontWeight : '400',
        color : 'black',

    },
    
    label : {
      color: "black",
      fontWeight: "bold", 
      marginRight : 20,
    },

    row :{
      flexDirection : 'row',
    }
})