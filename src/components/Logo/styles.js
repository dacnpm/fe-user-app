import {StyleSheet} from 'react-native'
export default  styles = StyleSheet.create({
    container : {
        justifyContent : 'center',
        alignItems : 'center',
        display : 'flex',
        marginBottom :10,
    },
    imageContainer :{
        width: 64,
        height: 64,
    },
    title : {
        fontWeight : 'bold',
        fontSize : 30,
    }
})