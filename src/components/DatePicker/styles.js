import {StyleSheet} from 'react-native'
import _styles from '../../styles/style'

const styles = StyleSheet.create({
    container : {
        textAlign : 'center',
        borderRadius : 30,
        justifyContent : 'center',
        alignItems : 'center',
    },
    title : {
        fontWeight : 'bold',
        fontSize : 30,
        textAlign : 'center',
        borderRadius : 20,
        color : 'black',
        display : 'flex',
    }

})


export default styles;