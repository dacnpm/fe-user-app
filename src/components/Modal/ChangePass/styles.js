import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container : {
        paddingTop : 10,
        // backgroundColor : '#e1e6ed',
        backgroundColor : 'white',
        // justifyContent : 'center',
        alignItems : 'center',
        height : 500,
        borderRadius : 50,
        borderWidth : 2,
    },

    title : {
        fontWeight : '900',
        fontSize : 30,
    },

    input : {
        marginVertical : 20,
        
        borderWidth : 2,
        borderRadius : 5,
        display : 'flex',
        width : 200,
    },

    groupButton : {
        paddingVertical : 20,
        display : 'flex',
        flexDirection : 'row',
        width : 250,
        justifyContent :'space-between'
        // justifyContent :'flex-start'
    },
    
    button : {
        fontSize : 20,
        fontWeight : 'bold',
        paddingVertical : 10,
        minWidth : 115,
        textAlign  : 'center',
    },

    confirm :{
        backgroundColor : "#10e04e"
    },

    cancel : {
        borderWidth : 1,
        borderTopWidth : 1.5,
        borderStartWidth : 1.5,
        borderColor : "#10e04e",
    }
    
});


export default styles;
