import React, {useState,useRef,useImperativeHandle} from 'react';
import {Button, Text, View,TextInput, Alert,SafeAreaView,TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles.js';
import Input from '../../Input';
import { forwardRef } from 'react';
import loginApi from '../../../api/loginApi'
import CustomInput from '../../Input'
function ChangePass(props,ref) {
  const [isModalVisible, setModalVisible] = useState(false);
  const [confirmCode,setConfirmCode] = useState("");
  const [pass,setPass] = useState("");
  const [confirmPass,setConFirmPass] = useState("");
  const {visible} = {props}
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  }
  const {email} = props;
  console.log(email);
  useImperativeHandle(ref,()=>({
    toggleModal : ()=>{
     setModalVisible(!isModalVisible);
    }
  }))
  const confirmOnClick = () =>{
      // loginApi.verifyEmail({email,code : confirmCode})
      // .then(res=>{
      //   //success
      //   console.log(res);
      //   if (res.isSuccess){
      //     Alert.alert("OK");
      //     setModalVisible(false);
      //   }
      //   else {
      //       Alert.alert("Fail");
           
      //   }
      // })
      // .catch(err=>{       
      //   console.log(err);

      // })
  }

  return (
    <View >
      <Modal isVisible={isModalVisible}>
        <View style={styles.container}>
            <SafeAreaView>
                <Text style={styles.title}>Mã xác nhận:</Text>
                <TextInput style= {styles.input}
                    onChangeText = {setConfirmCode} 
                    placeholder = {"Mã xác nhận"}
                    autoFocus = {true}

                />
            {/* <CustomInput label ={"Mật khẩu"} iconName = {"key-outline"} secureTextEntry= {true}  onChangeText = {setPass} ></CustomInput> */}
            {/* <CustomInput label ={"Xác nhận mật khẩu"} placeholder= {"Nhập lại Mật khẩu"} iconName = {"key-outline"} secureTextEntry= {true}  onChangeText = {setConFirmPass} ></CustomInput> */}
            </SafeAreaView>
            <View style = {styles.groupButton}>
                <TouchableOpacity
                    onPress = {confirmOnClick}
                >
                    <Text style = {{...styles.button,...styles.confirm}}>
                        Xác nhận
                    </Text>
               </TouchableOpacity>

                <TouchableOpacity
                    onPress = {toggleModal}
                >
                    <Text style = {{...styles.button,...styles.cancel}}>
                        Hủy bỏ
                    </Text>
               </TouchableOpacity>

            </View>
        </View>
      </Modal>
    </View>
  );
}
ChangePass  = forwardRef(ChangePass)

export default ChangePass;
