import React, {useState} from 'react';
import {Button, Text, View,TextInput, Alert,SafeAreaView,TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import styles from './styles';
import Input from '../../Input';
import { forwardRef } from 'react';
import loginApi from '../../../api/loginApi'
import CustomInput from '../../Input'
export default function (props) {
  const {isShow,setIsShow} = props;
  const {navigation} = props.navigation;
  const confirmOnClick = () =>{
      //navigate screen
      setIsShow(false);
      console.log("xxx",navigation);
      navigation.navigate("DailyHealth");
  }

  const toggleModal = ()=>{
    setIsShow(false)

  }

  return (
    <View >
      <Modal isVisible={isShow}>
        <View style={styles.container}>
            <Text style = {styles.title}>
              Dựa vào khai báo lịch trình, bạn thuộc diện bắt buộc xét nghiệm.Vui lòng đến mục xét nghiệm
            </Text>
            <View style = {styles.groupButton}>
                <TouchableOpacity
                    onPress = {confirmOnClick}
                >
                    <Text style = {{...styles.button,...styles.confirm}}>
                        Xác nhận
                    </Text>
               </TouchableOpacity>

                <TouchableOpacity
                    onPress = {toggleModal}
                >
                    <Text style = {{...styles.button,...styles.cancel}}>
                        Hủy bỏ
                    </Text>
               </TouchableOpacity>

            </View>
        </View>
      </Modal>
    </View>
  );
}

