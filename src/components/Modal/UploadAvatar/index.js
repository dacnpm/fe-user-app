import React, {useState} from 'react';
import { Text, View,TextInput, Alert,SafeAreaView,TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import DocumentPicker from 'react-native-document-picker';
import Toast from 'react-native-simple-toast';
import { RNCamera } from 'react-native-camera';
import { useCamera } from 'react-native-camera-hooks';

import Button from '../../Button'
import styles from './styles';
import Input from '../../Input';
import { forwardRef } from 'react';
import loginApi from '../../../api/loginApi'
import CustomInput from '../../Input'
import { Image } from 'react-native';
import AsyncStorage from '../../../storage'
import userApi from '../../../api/userApi';
export default function (props) {
  const [singleFile, setSingleFile] = useState(null);

  const {isShow,setIsShow,mustUpdate,setMustUpdate,avatarURL} = props;
  const onSubmit = () =>{
      const maxFile = 10*1024*1024;
      if (singleFile.size>maxFile){
        Toast.show("Dung lượng file ảnh không quá 10MB",Toast.LONG);
        return;
      }
      else 
      if(singleFile==null){
        Toast.show("Vui lòng chọn ảnh");
        return;
      }
      else{
        AsyncStorage.read()
          .then(token=>userApi.UploadAvatar(token,singleFile))
          .then(res=>{
            console.log(res);
            Toast.show("Cập nhật ảnh đại diện thành công",Toast.SHORT);
            setIsShow(false);
            setMustUpdate(!mustUpdate)
          })
          .catch(err=>{
            console.log(err);
            Toast.show("Cập nhật ảnh đại diện không thành công",Toast.LONG);
          });
      }
  }
  const onCancel = ()=>{
    setIsShow(false);
  }

  function  onFilePicker() {
   DocumentPicker.pick({
      type : [DocumentPicker.types.images],
    })
      .then(res=>{
          setSingleFile(res);
          console.log(res);
      })

      .catch(err=>{
          const {data} = err;
          console.log("ERR",data);
      })
  }
  function onOpenCamera() {
    // Alert.alert("OK");
    takePicture();
  }

  async function takePicture(camera) {
    const options = { quality: 0.5, base64: true };
    const data = await camera.takePictureAsync(options);
    //  eslint-disable-next-line
    console.log(data.uri);
  }

  const PendingView = () => (
    <View
      style={{
        flex: 1,
        backgroundColor: 'lightgreen',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      <Text>Waiting</Text>
    </View>
  );
  

  return (
    <View >
      <Modal isVisible={isShow}>

        <View style={styles.container}>
            <Text style ={styles.header}>Thay đổi ảnh đại diện</Text>
            <View style = {styles.row_center}>
              <TouchableOpacity
                onPress = {onFilePicker}
              >
                <View style = {styles.row_center}>
                  <Text style = {styles.title}>Chọn ảnh</Text>
                  <Image
                    source = {require('../../../assets/change.png')}
                    style = {styles.icon}
                  />
                </View>
              </TouchableOpacity>


            </View>
            <View style = {styles.previewContainer}>
              {singleFile!= null? 
                <Image
                  source = {{uri : singleFile.uri}}
                  style = {styles.previewImage}
                /> 
                :
                <Image
                  source = {{uri : avatarURL}}
                  style = {styles.previewImage}

                /> 
                }
              </View>
            <View style = {styles.groupButton}>
               <Button
                    title = {"Cập nhật"}
                    onPress = {onSubmit}
                    titleStyle = {styles.submitButton}

                />

                <Button
                    title = {"Quay lại"}
                    onPress = {onCancel}
                    titleStyle = {styles.backButton}
                />

            </View>
        </View>
      </Modal>
    </View>
  );
}

