import React,{useEffect,useState} from 'react';
import {
    Image, Text,


    TouchableOpacity, View
} from 'react-native';
import styles from './style';


export default function NewItem(props){
    let {onClick,item} = props;
    // console.log(item.content);
    // console.log("xxx",item.content);
    const end = item.content.indexOf("\n");
    // console.log(end);
    const shortContent = item.content.substring(0,end-2);
    // console.log("SHORT =",shortContent);
 
    return (
        <TouchableOpacity activeOpacity = {0.6}
            onPress = {
                onClick
                }
            
        >
            <View style = {styles.container}>
                <Text style ={styles.title}>
                     {item.title}
                 </Text>
                <Image 
                    style = {styles.categoryImage}
                    source={{uri: item.image}}

                />
               
                <Text style={styles.shortContent}>
                    {shortContent+"....."}
                </Text>
                
                
            </View>
        </TouchableOpacity>
    )
}
