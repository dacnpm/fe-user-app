import React, { Component, useState } from 'react';
import { Image, ImageBackground, Text, View } from 'react-native';
import styles  from './styles';
import { Card, ListItem, Button, Icon } from 'react-native-elements'
import Collapsible from 'react-native-collapsible';
import Accordion from 'react-native-collapsible/Accordion';

import { Alert } from 'react-native';

import TimeFormat from '../../utils/time'

export default function (props){
    const {item} = props;
    const time = new TimeFormat();
    let result;
    result = item.testingState;
    console.log(item);
        switch (item.result) {
            case "Positive":
                result = "Dương tính"
                break;
        
            case "Negative":
                result = "Âm tính"
                break;
        
            default:
                break;
        }
    function convert(item){
        const [time1,thu,ngay] = time.convertStringToTime(item);
        return `${time1}, ${thu}, Ngày ${ngay}`
    }

    const [activeSections,setActiveSections] = useState([]);
    const _renderSectionTitle = (section) => {
        return (
          <View style={{backgroundColor : 'blue'}}>
            <Text>{section.content}</Text>
          </View>
        );
      };

      const _renderHeader = (item) => {
        return (
            <View style = {styles.headerBackground}>     

                <View style = {styles.row} >
                    <View style={styles.row1}>
                        <Text style ={styles.title}>Xét nghiệm: </Text>
                    </View>

                    <View  style = {styles.row2}>
                        <Text style = {styles.data}>{convert(item.testingDate)}</Text>
                    </View>
                </View>

                <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Kết quả: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data,styles.result}>{result}</Text>
                </View>
            </View>

            </View>
        );
      };
      const _renderContent = (section) => {
        return (
        <View >
            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Ngày đăng kí: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data}>{convert(item.testingDate)}</Text>
                </View>
            </View>
            
            {/* <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Ngày test: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data}>{convert(item.testingDate)}</Text>
                </View>
            </View> */}

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Địa điểm: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data}>{item.testingLocation.name +","+item.testingLocation.address}</Text>
                </View>
            </View>

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Giá tiền: </Text>
                </View>

                <View  style = {styles.row2}>
                    <View style ={{flexDirection : 'row'}}> 
                        <Text style = {styles.data}>{item.cost + " VND   " }</Text>
                        
                        {!item.isPaid ? 
                                (<ImageBackground
                                source = {require('../../assets/close.png')}
                                style = {{...styles.image,opacity : 0.6}}
                            >

                            <Image
                                style = {styles.image}
                                source = {require('../../assets/paid.png')}
                            />
                            </ImageBackground>)

                            : 
                            <Image
                                style = {styles.image}
                                source = {require('../../assets/paid.png')}
                            />
                        }

                    </View>
                </View>
            </View>

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Trạng thái: </Text>
                </View>

                <View  style = {styles.row2}>
                    <View style ={{flexDirection : 'row'}}> 
                        <Text style = {styles.data}>{item.testingState}</Text>

                    </View>
                </View>
            </View>


        </View> );
      };

      const _updateSections = (activeSections) => {
        setActiveSections(activeSections);
      };
      


    return (
        <View style = {styles.container}>     
         {/* 
            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Ngày đăng kí: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data}>{convert(item.testingDate)}</Text>
                </View>
            </View>
            
            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Ngày test: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data}>{convert(item.testingDate)}</Text>
                </View>
            </View>

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Địa điểm: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data}>{item.testingLocation.name +","+item.testingLocation.address}</Text>
                </View>
            </View>

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Giá tiền: </Text>
                </View>

                <View  style = {styles.row2}>
                    <View style ={{flexDirection : 'row'}}> 
                        <Text style = {styles.data}>{item.cost + " VND   " }</Text>
                        
                        {!item.isPaid ? 
                                (<ImageBackground
                                source = {require('../../assets/close.png')}
                                style = {{...styles.image,opacity : 0.6}}
                            >

                            <Image
                                style = {styles.image}
                                source = {require('../../assets/paid.png')}
                            />
                            </ImageBackground>)

                            : 
                            <Image
                                style = {styles.image}
                                source = {require('../../assets/paid.png')}
                            />
                        }

                    </View>
                </View>
            </View>

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Trạng thái: </Text>
                </View>

                <View  style = {styles.row2}>
                    <View style ={{flexDirection : 'row'}}> 
                        <Text style = {styles.data}>{item.testingState}</Text>

                    </View>
                </View>
            </View>

            <View style = {styles.row} >
                <View style={styles.row1}>
                    <Text style ={styles.title}>Kết quả: </Text>
                </View>

                <View  style = {styles.row2}>
                    <Text style = {styles.data,styles.result}>{result}</Text>
                </View>
           </View> */}
            <Accordion
                sections = {[item]}
                activeSections = {activeSections}
                renderContent = {_renderContent}
                renderHeader = {_renderHeader}
                onChange = {_updateSections}
                duration = {1000}
                expandMultiple = {true}
                align = {'top'}
                underlayColor = {"#c4c2c0"}
            />
        </View>
    )
}