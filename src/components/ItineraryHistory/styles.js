import {StyleSheet} from 'react-native'
import { renderNode } from 'react-native-elements/dist/helpers';
import _styles from '../../styles/style'

const image = 40;

const styles = StyleSheet.create({
    container : {
        flex : 1,
        backgroundColor : 'rgba(173, 156, 154,0.06)',
        // height : 300,
        flexDirection : 'row',
        // paddingVertical : 20,
        marginVertical : 10,
        paddingVertical : 5,
        // borderTopWidth : 2,
        // borderBottomWidth : 2,
        // borderColor : 'blue'
    },
    


    icon : {
        width : image,
        height : image,
        paddingHorizontal : 10
    },

    time : {
        // borderWidth : 1,
        // backgroundColor : 'rgba(212, 42, 45,0,5)',
        // backgroundColor : 'red',
        borderRadius : 10,
        marginRight : image/2
    },


    box: {
        ..._styles.backgroundBorder,
        ..._styles.box,
        borderRadius : 30,
        borderWidth : 0.5,
        flex : 3,
        alignItems : 'center',
        // justifyContent : 'space-around',
        paddingHorizontal : 10,
        minHeight : 200,
    },

    arrow : {
        flex : 1,
        // backgroundColor : 'white',
        justifyContent : 'center',
        alignItems : 'center'
    },

    row :{
        flexDirection : 'column',
        flex : 1,
        // backgroundColor : 'red',
        justifyContent : 'center',
        alignItems : 'center'
    },

    title :{
        fontSize : 22,
        fontWeight : 'bold'
    },

    rowFlight : {
        flexDirection : 'row',
        alignItems : 'center',
    },

    flightText :{
        fontSize : 18,
        // marginLeft : 10,
    }


})


export default styles;