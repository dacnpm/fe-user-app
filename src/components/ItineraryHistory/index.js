import React, { useState } from 'react';
import { Text, View,TouchableOpacity,Image } from 'react-native';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers
  } from 'react-native-popup-menu';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-simple-toast';
import styles from './styles';
import Time from '../../utils/time'
import EditItinerary from '../Modal/EditItinerary';
import userApi from '../../api/userApi';
import AsyncStorage from '../../storage';
import time2 from '../../utils/time2';


export default function(props){
    const [loading,setLoading] = useState(false);
    const [isShow,setIsShow] = useState(false);
    const {listTime,item,mustUpdate,setMustUpdate} = props;
    // const departureDate = time.convertStringToTime(item.departureTime);
    const myDepartureTime = new Date(item.departureTime);
    const myLandingTime = new Date(item.landingTime);
    const departureDate = time2.convertDateToDate(myDepartureTime);
    const landingDate =   time2.convertDateToDate(myLandingTime);
    // const landingDate =   time.convertStringToTime(item.landingTime);
    const {tabBarNavigation} = props;
    const body = (
        <View style = {styles.container}>
        <View style = {styles.box}>
            <View style = {styles.row}>
                <Image
                    style = {styles.icon}
                    source = {require('../../assets/clock.png')}
                />
                <Text style = {styles.title}>{myDepartureTime.toTimeString().substr(0,5)}</Text>
                <Text>{departureDate[0] + ", ngày "+ departureDate[1]}</Text>
            </View>

            <View style = {styles.row}>
                <View style = {styles.rowFlight}>
                    <Image
                        style = {styles.icon}
                        source = {require('../../assets/departure.png')}
                    />
                    <Text style = {styles.flightText}>{" - "+item.travelNo}</Text>
                </View>
                <Text style = {styles.title}>{item.departure}</Text>
            </View>
        </View>
        <View style = {styles.arrow}>
           <Image
                source = {require('../../assets/right-arrow.png')}
                resizeMode = 'stretch'
                style = {{width : 40,height :50}}
           />
        </View>

        <View style = {styles.box}>
            <View style = {styles.row}>
                <Image
                    style = {styles.icon}
                    source = {require('../../assets/clock-landing.png')}
                />
                <Text style = {styles.title}>{myLandingTime.toTimeString().substr(0,5)}</Text>
                <Text>{landingDate[0] + ", ngày "+ landingDate[1]}</Text>
            </View>

            <View style = {styles.row}>
            <View style = {styles.rowFlight}>
                    <Image
                        style = {styles.icon}
                        source = {require('../../assets/landing.png')}
                    />
                </View>
                <Text style = {styles.title}>{item.destination}</Text>
            </View>
        </View>

    </View>
    )

    function onSelect(value){
        switch (value) {
            case 1:
                setIsShow(true);
                break;
        
            case 2:
                setLoading(true);
                AsyncStorage.read()
                    .then(token=>{
                        return userApi.DeleteItinerary(item.id,token);
                    })
                    .then(res=>{
                        console.log("DELETE:",res);
                        setMustUpdate(!mustUpdate)
                        Toast.show("Xóa lịch trình thành công",Toast.SHORT)
                    })
                    .catch(err=>console.log(err))
                    .finally(()=>{
                        setLoading(false);
                    })
                break;
        
            default:
                break;
        }
    }
    return (
        <View>
        <Spinner
            visible={loading}
         />
         <EditItinerary
            listTime = {listTime}
            isShow = {isShow}
            setIsShow = {setIsShow}
            item = {item}
            setLoading = {setLoading}
            mustUpdate = {mustUpdate}
            setMustUpdate = {setMustUpdate}
            tabBarNavigation = {tabBarNavigation}
         />
        <Menu renderer = {renderers.Popover} onSelect = {onSelect}    >
            <MenuTrigger triggerOnLongPress = {true}  children = {body}  />
            <MenuOptions   >
                <MenuOption value={1}   >
                    <Text style = {styles.title}>Chỉnh sửa</Text>
                </MenuOption>
                <MenuOption value={2}>
                    <Text  style = {styles.title} >Xóa</Text>
                </MenuOption>
            </MenuOptions>
        </Menu>
    </View>
    )
}
