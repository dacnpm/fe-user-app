import React, { useState } from 'react';
import { Text, View,TouchableOpacity,Image, Alert } from 'react-native';
import styles from './styles';
import { TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {
    Menu,
    MenuOptions,
    MenuOption,
    MenuTrigger,
    renderers
  } from 'react-native-popup-menu';
import Spinner from 'react-native-loading-spinner-overlay';

import Toast from 'react-native-simple-toast';

import TimeFormat from '../../utils/time'
import userApi from '../../api/userApi';
import AsyncStorage from '../../storage'
import EditCheckin from '../Modal/EditCheckin';
import time2 from '../../utils/time2';
export default function(props){
    const { Popover } = renderers;
    const [loading,setLoading] = useState(false);
    const [isShow,setIsShow] = useState(false);
    let timeformat = new TimeFormat();
    const {item} = props;
    const [time,thu,ngay] = timeformat.convertStringToTime(item.time);
    const {mustUpdate,setMustUpdate} = props;
    const datetime = new Date(item.time);
    const list = time2.convertDateToDate(datetime)
    console.log(list);
    const body =  
        (<View style = {styles.container}>
            <View style = {styles.col1}>
                <View style = {styles.iconContainer}>
                    {/* <Icon name={"location"} size={25} color="#f21f27"/> */}
                    <Image
                        source = {require('../../assets/map.png')}
                        style = {styles.image}
                    />
                </View>
                <Text style = {styles.address}>{item.address}</Text>
            </View>

            <View style= {styles.col2}>
                <View style = {styles.thoigian}>
                <Text style= {styles.thoigianText} >{datetime.toTimeString().substr(0,5)}</Text>
                </View>

                <View style = {styles.ngay}>
                    <Text>{list[0]}</Text>
                    <Text>{list[1]}</Text>
                </View>
        
            </View>
        </View>)



    function onSelect(value){
        //get API
        switch (value) {
            // Edit
            case 1:
                setIsShow(true);
                // setLoading(false)
                break;
            case 2 :
                setLoading(true);
                AsyncStorage.read()
                .then(token=>{
                    return userApi.DeleteCheckIn(item.id,token);
                })
                .then(res=>{
                    if (res.isSuccess){
                        Toast.show('Xoá thành công', Toast.SHORT);
                        setMustUpdate(!mustUpdate);
                    }
                    else {
                        Toast.show('Lỗi!!!', Toast.LONG);
                    }

                })
                .catch(err=>console.log(err))
                .finally(()=>{
                    setLoading(false);
                })
                break;
            default:
                break;
        }
        
    }

    return (
  
        <View>
            <Spinner
                visible={loading}
             />
             <EditCheckin
                isShow = {isShow}
                setIsShow = {setIsShow}
                item = {item}
                setLoading = {setLoading}
                mustUpdate = {mustUpdate}
                setMustUpdate = {setMustUpdate}
             />
            <Menu renderer = {renderers.Popover} onSelect = {onSelect}    >
                <MenuTrigger triggerOnLongPress = {true}  children = {body}  />
                <MenuOptions   >
                    <MenuOption value={1}   >
                        <Text style = {styles.title}>Chỉnh sửa</Text>
                    </MenuOption>
                    <MenuOption value={2}>
                        <Text  style = {styles.title} >Xóa</Text>
                    </MenuOption>
                </MenuOptions>
            </Menu>
        </View>
    )
}
