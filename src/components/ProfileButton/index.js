import React,{useEffect,useState} from 'react';
import {
    Image, Text,


    TouchableOpacity, View
} from 'react-native';
import style from '../NewsItem/style';
import styles from './style';


export default function NewItem(props){
    const {onClick,item} = props;
    const sourceString = "../../assets/"+ item.imageName+".png";
    let image;
    switch (item.imageName) {
        case "manageProfile":
            image = require('../../assets/manageProfile.png')
            break;
        case "changePassword":
            image = require('../../assets/changePassword.png')
            break;
        case "movingHistory":
            image = require('../../assets/movingHistory.png')
            break;
        case "healthDeclare":
            image = require('../../assets/healthDeclare.png')
            break;
        case "logout":
            image = require('../../assets/logout.png')
            break;
    
        default:
            break;
    }
    return (
        <TouchableOpacity activeOpacity = {0.6}
            onPress = {
                onClick
                }
        >
            <View style = {styles.container}>
                <View style = {styles.col1}>
                    <Image
                        style  = {styles.imageContainer}
                        source = {image}
                    />
                </View>

                <View style ={styles.col2}>
                     <Text style={styles.title}>{item.name}</Text>
                </View>

                <View>
                    <Image
                        style = {styles.icon}
                        source = {require('../../assets/arrow.png')}
                    />
                </View>
                
            </View>
        </TouchableOpacity>
    )
}
